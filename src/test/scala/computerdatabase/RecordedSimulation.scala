package computerdatabase

import scala.concurrent.duration._

import io.gatling.core.Predef._
import io.gatling.http.Predef._
import io.gatling.jdbc.Predef._

import scala.util.Random

class RecordedSimulation extends Simulation {

	// Set LoadTest duration in seconds:
	private val total_duration_sec = 20 * 60

	// Set LoadTest profile (VUs and Pacing bounds):
	private val profile = Map(
		"UC_01" -> Map(
			"VUs" -> 10,
			"Pacing_min" -> 48,
			"Pacing_max" -> 96
		),
		"UC_02" -> Map(
			"VUs" -> 6,
			"Pacing_min" -> 48,
			"Pacing_max" -> 96
		),
		"UC_03" -> Map(
			"VUs" -> 2,
			"Pacing_min" -> 48,
			"Pacing_max" -> 96
		),
		"UC_04" -> Map(
			"VUs" -> 2,
			"Pacing_min" -> 48,
			"Pacing_max" -> 96
		)
	)

	// Common HTTP protocol params:
	val httpProtocol = http
		.baseUrl("http://computer-database.gatling.io")
		.inferHtmlResources()
		.doNotTrackHeader("1")
		.userAgentHeader("Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36")

	// Header for HTML content:
	val headers_html = Map(
		"Accept" -> "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
		"Accept-Encoding" -> "gzip, deflate",
		"Accept-Language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
		"Origin" -> "http://computer-database.gatling.io",
		"Pragma" -> "no-cache",
		"Upgrade-Insecure-Requests" -> "1")

	// Header for CSS content:
	val headers_css = Map(
		"Accept" -> "text/css,*/*;q=0.1",
		"Accept-Encoding" -> "gzip, deflate",
		"Accept-Language" -> "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
		"Pragma" -> "no-cache")

	// Fetch computers model params from local dataset:
	val records = tsv("records.dat").shuffle

	// Generate random sequence for page access:
	val randomPages = Iterator.continually(
		Map("page" -> Random.nextInt(100))
	)

	// UC_01 - Main page open
	val scn_01 = scenario("UC_01")
    .during(total_duration_sec) {
		pace(min=profile("UC_01")("Pacing_min"), max=profile("UC_01")("Pacing_max"))
		// Main page
		.exec(
			http("computers")
			.get("/computers")
			.headers(headers_html)
			.resources(
				http("main.css")
				.get("/assets/stylesheets/main.css")
				.headers(headers_css),
	            http("bootstrap.min.css")
				.get("/assets/stylesheets/bootstrap.min.css")
				.headers(headers_css)
			)
			.check(status.is(200))
		)
	}

	// UC_02 - Search and view element
	val scn_02 = scenario("UC_02")
    .during(total_duration_sec) {
		pace(min=profile("UC_02")("Pacing_min"), max=profile("UC_02")("Pacing_max"))
		.feed(randomPages)
		// Search
		// CAUTION: Search by company name can return empty page
		/*
		.feed(records)
		.exec(
			http("computers_search")
			.get("/computers?f=${company}")
			.headers(headers_html)
			.resources(
				http("main.css")
				.get("/assets/stylesheets/main.css")
				.headers(headers_css),
            	http("bootstrap.min.css")
				.get("/assets/stylesheets/bootstrap.min.css")
				.headers(headers_css)
			)
			.check(status.is(200))
			.check(regex("""<td><a href="/computers/(\d+)">""").findRandom.optional.saveAs("elementId"))
		)
		.exec( session => {
			println("Search for company: " + session("company").as[String])
			session
		})
		*/
		// Alternative search is by page number
		.exec(
			http("computers_search")
			.get("/computers?p=${page}")
			.headers(headers_html)
			.resources(
				http("main.css")
				.get("/assets/stylesheets/main.css")
				.headers(headers_css),
            	http("bootstrap.min.css")
				.get("/assets/stylesheets/bootstrap.min.css")
				.headers(headers_css)
			)
			// fetch random element id from page:
			.check(regex("""<td><a href="/computers/(\d+)">""").findRandom.optional.saveAs("elementId"))
			.check(status.is(200))
		)
		// Log to console:
		.exec(
			session => {
				println("Open element page by ID: " + session("elementId").as[String])
				session
			}
		)
		// Open element by ID (if found)
		.doIf("${elementId.exists()}") {
			pause(3)
			.exec(
				http("computers_view")
				.get("/computers/${elementId}")
				.headers(headers_html)
				.resources(
					http("main.css")
					.get("/assets/stylesheets/main.css")
					.headers(headers_css),
					http("bootstrap.min.css")
					.get("/assets/stylesheets/bootstrap.min.css")
					.headers(headers_css)
				)
				.check(status.is(200))
			)
		}
	}

	// UC_03 - Delete element
	val scn_03 = scenario("UC_03")
    .during(total_duration_sec) {
		pace(min=profile("UC_03")("Pacing_min"), max=profile("UC_03")("Pacing_max"))
		.feed(randomPages)
		// Search
		// CAUTION: Search by company name can return empty page
		/*
		.feed(records)
		.exec(
			http("computers_search")
			.get("/computers?f=${company}")
			.headers(headers_html)
			.resources(
				http("main.css")
				.get("/assets/stylesheets/main.css")
				.headers(headers_css),
            	http("bootstrap.min.css")
				.get("/assets/stylesheets/bootstrap.min.css")
				.headers(headers_css)
			)
			.check(status.is(200))
			.check(regex("""<td><a href="/computers/(\d+)">""").findRandom.optional.saveAs("elementId"))
		)
		.exec( session => {
			println("Search for company: " + session("company").as[String])
			session
		})
		*/
		// Alternative search is by page number
		.exec(
			http("computers_search")
			.get("/computers?p=${page}")
			.headers(headers_html)
			.resources(
				http("main.css")
				.get("/assets/stylesheets/main.css")
				.headers(headers_css),
            	http("bootstrap.min.css")
				.get("/assets/stylesheets/bootstrap.min.css")
				.headers(headers_css)
			)
			// fetch random element id from page:
			.check(regex("""<td><a href="/computers/(\d+)">""").findRandom.optional.saveAs("elementId"))
			.check(status.is(200))
		)
		// Log to console:
		.exec(
			session => {
				println("Deleted element ID: " + session("elementId").as[String])
				session
			}
		)
		// Delete element (if found)
		.doIf("${elementId.exists()}") {
			pause(3)
			.exec(
				http("computers_delete")
				.post("/computers/${elementId}/delete")
				.headers(headers_html)
				.resources(
					http("main.css")
					.get("/assets/stylesheets/main.css")
					.headers(headers_css),
					http("bootstrap.min.css")
					.get("/assets/stylesheets/bootstrap.min.css")
					.headers(headers_css)
				)
				.check(status.is(200))
			)
		}
	}

	// UC_04 - Add element
	val scn_04 = scenario("UC_04")
    .during(total_duration_sec) {
		pace(min=profile("UC_04")("Pacing_min"), max=profile("UC_04")("Pacing_max"))
		.feed(records)
		// Go to creation form
		.exec(
			http("computers_new")
			.get("/computers/new")
			.headers(headers_html)
			.resources(
				http("main.css")
				.get("/assets/stylesheets/main.css")
				.headers(headers_css),
            	http("bootstrap.min.css")
				.get("/assets/stylesheets/bootstrap.min.css")
				.headers(headers_css)
			)
			.check(status.is(200))
		)
		.pause(3)
		// Create new element
		.exec(
			http("computers_add")
			.post("/computers")
			.headers(headers_html)
			.formParam("name", "${model}")
			.formParam("introduced", "${introduced}")
			.formParam("discontinued", "${discontinued}")
			.formParam("company", "${company_id}")
			.check(status.is(200))
		)
		// Log to console:
		.exec( session => {
			println("Created Model: " + session("model").as[String])
			session
		})
	}

	// Setting up the loadtest with defined scenarios:
	setUp(
		scn_01.inject(rampUsers(profile("UC_01")("VUs")) during(1 minute)).protocols(httpProtocol),
		scn_02.inject(rampUsers(profile("UC_02")("VUs")) during(1 minute)).protocols(httpProtocol),
		scn_03.inject(rampUsers(profile("UC_03")("VUs")) during(1 minute)).protocols(httpProtocol),
		scn_04.inject(rampUsers(profile("UC_04")("VUs")) during(1 minute)).protocols(httpProtocol)
	)
}
