# Создание проекта Gatling

## Из шаблона

Как создать проект Gatling по шаблону http://www.foundweekends.org/giter8/:

```
sbt new gatling/gatling.g8
```

## Из репозитория

Source: https://github.com/gatling/gatling-sbt-plugin-demo

A simple project showing how to configure and use Gatling's SBT plugin to run Gatling simulations.

This project uses SBT 1, which is available [here](https://www.scala-sbt.org/download.html).

Get the     project

    git clone https://github.com/gatling/gatling-sbt-plugin-demo.git && cd gatling-sbt-plugin-demo

Start SBT

    $ sbt

Run all simulations

    > gatling:test

Run a single simulation

    > gatling:testOnly computerdatabase.BasicSimulation

List all tasks

    > tasks gatling -v
